(define-module (raingloom packages idris)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages idris)
  #:use-module (gnu packages chez)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages multiprecision))

(define-public idris2
  (let ((commit "a87a3c14c29a0d1333cb25628409ba2609284b86")
        (revision "0"))
  (package
   (name "idris2")
   (version (string-append "0.1-" revision "." (substring commit 0 7)))
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/edwinb/Idris2/")
		  (commit commit)))
	    (sha256 (base32 "16icgab445d3cqas7in21gv93zrcjb1jlw8mk1dxmifi0n7h7jip"))))
   (build-system gnu-build-system)
   (arguments
     `(#:phases
       (modify-phases
	%standard-phases
	(replace
	 'configure
	 (lambda _
	   (let ((out (assoc-ref %outputs "out")))
	     (setenv "PREFIX" out)
	     (setenv "IDRIS_CC" "clang")
	     #t)))
	(replace
	 'build
	 (lambda _
	   (invoke "make" (string-append "PREFIX=" (getenv "PREFIX")) "src/YafflePaths.idr" "check_version")
	   
	   ;; save codegen result so if the linker fails we don't have to wait for the type checker and the compiler every time
	   (chdir "src")
	   (invoke "idris" "--codegenonly" "--partial-eval" "--output" "../idris2.c" "Idris/Main.idr")
	   (chdir "..")

	   (let* ((lib (getenv "IDRIS_LIBRARY_PATH"))
		  (rts (string-append lib "/rts/"))
		  ;; clang will warn about some of the options having no effect when compiling or linking, just ignore them
		  (cc
		   (lambda args
		     (apply
		      invoke
		      (append
		       `(,(getenv "IDRIS_CC")
			 
			 "-fwrapv"
			 "-fno-strict-overflow"
			 "-std=c99"
			 "-pipe"
			 "-fdata-sections"
			 "-ffunction-sections"

			 "-D_POSIX_C_SOURCE=200809L"
			 "-DHAS_PTHREAD"
			 "-DIDRIS_ENABLE_STATS"
			 "-DIDRIS_GMP"
			 
			 "-Wl,-gc-sections"
			 ,(string-append "-L" rts)
			 
			 "-lidris_rts"
			 "-lgmp"
			 "-lpthread"
			 "-lm"
			 
			 ;"-I."
			 ,(string-append "-I" rts)
			 ,(string-append "-I" lib "/prelude")
			 ,(string-append "-I" lib "/base"))
		       args)))))
	     ;; compile to object file (so that we don't have to wait for the optimizer to retry linking
	     (cc "-c" "-O2" "-o" "idris2.o" "idris2.c")
	     ;; link with the rest of the RTS
	     (cc
	      "-o" "idris2"
	      
	      "idris2.o"
	      (string-append rts "/idris_main.c")
	      (string-append rts "/idris_stats.c")
	      (string-append rts "/idris_opts.c"))
	     (error "because we want to keep the build directory"))))
	(replace
	 'install
	 (lambda _
	   (invoke "make" (string-append "PREFIX=" (getenv "PREFIX")) "install"))))))
   (inputs
    `(("clang" ,clang)
      ("idris" ,idris)
      ("gmp" ,gmp)
      ("chez-scheme" ,chez-scheme)))
   (home-page "https://idris-lang.org/")
   (synopsis "A dependently typed programming language, a successor to Idris")
   (description "This is a pre-alpha implementation of Idris 2, the successor to Idris.")
   (license license:bsd-3))))
