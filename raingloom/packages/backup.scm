(define-module (raingloom packages backup)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system qt)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages qt))

(define-public tarsnap
  (package
   (name "tarsnap")
   (version "1.0.39")
   (source
	(origin
	 (method url-fetch)
	 (uri (string-append
		   "https://www.tarsnap.com/download/tarsnap-autoconf-"
		   version ".tgz"))
	 (sha256
	  (base32
	   "10i0whbmb345l2ggnf4vs66qjcyf6hmlr8f4nqqcfq0h5a5j24sn"))))
   (build-system gnu-build-system)
   (arguments
	'(#:phases
	  (modify-phases
	   %standard-phases
	   (add-after 'unpack 'patch-command-p
				  (lambda _
					(substitute* "Makefile.in"
								 (("command -p ") "")))))))
   (inputs
	`(("openssl" ,openssl)
	  ("zlib" ,zlib)))
   (native-inputs
	`(("e2fsprog" ,e2fsprogs)))
   (home-page "https://tarsnap.com")
   (synopsis "tarsnap client")
   (description "Tarsnap is a secure, efficient online backup service")
   (license #f ;;TODO: some kind of semi-open thing?
			)))

(define-public tarsnap-gui
  (package
   (name "tarsnap-gui")
   (version "1.0.2")
   (source
	(origin
	 (method url-fetch)
	 (uri (string-append
		   "https://github.com/Tarsnap/tarsnap-gui/archive/v"
		   version ".tar.gz"))
	 (sha256
	  (base32
	   "1biyr0jbf1vrz98qnvjy2ij7sp3qh9myfbnnsnrvw2xw993iy9rv"))))
   (build-system qt-build-system)
   (arguments
	'(#:tests? #f
	  #:phases
	  (modify-phases
		  %standard-phases
		(replace 'configure
		  (lambda _
			(invoke "qmake")))
		(add-after 'install 'actually-install-binary
		  (lambda* (#:key outputs #:allow-other-keys)
			(let ((bin (string-append
						   (assoc-ref outputs "out")
						   "/bin/")))
			  (install-file "tarsnap-gui" bin)
			  (chmod (string-append bin "/tarsnap-gui") #o755)))))))
   (inputs
	`(("qtbase" ,qtbase)))
   (propagated-inputs
	`(("tarsnap" ,tarsnap)))
   (home-page "https://tarsnap.com")
   (synopsis "Tarsnap GUI client")
   (description "The Tarsnap graphical user interface desktop
application is an open source cross-platform front-end to the
popular Tarsnap backup service. The application uses the tarsnap
command-line client utilities to communicate with the Tarsnap service
and thus they need to be installed on your system.")
   (license #f ;;TODO: BSD 2 clause?
			)))
