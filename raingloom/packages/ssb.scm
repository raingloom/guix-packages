(define-module (raingloom packages ssb))
(use-modules
 (gnu packages crypto)
 (gnu packages)
 (guix packages)
 (guix git-download)
 (guix build-system gnu))

(define-public sbotc
  (let ((commit "cfb1d40d73bdbd1a8d226f900ed23d87744fc022")
        (revision "0"))
    (package
     (name "sbotc")
     (version (string-append "0.1-" revision "." (substring commit 0 7)))
     (source (origin
			  (method git-fetch)
			  (uri
			   (git-reference
				(url
				 (string-append
				  "https://git.scuttlebot.io/"
				  "%25133ulDgs%2FoC1DXjoK04vDFy6DgVBB%2FZok15YJmuhD5Q%3D"
				  ".sha256"))
				(commit commit)))
			  (sha256
			   (base32
				"0iwdq9rgl6bgv4pq8ym94fiky1ri8w9mgnywxwj35k2ph6vm78rd"))))
     (build-system gnu-build-system)
     (arguments
      '(#:tests? #f
		#:phases
		(modify-phases
		 %standard-phases
		 (replace 'configure
				  (lambda _
					(setenv "CC" "gcc")))
		 (replace 'install
				  (lambda* (#:key outputs #:allow-other-keys)
					(invoke "make"
							(string-append
							 "PREFIX="
							 (assoc-ref outputs "out"))
							"install"))))))
     (inputs
      `(("libsodium" ,libsodium)))
     (synopsis "A command-line SSB client in C")
     (description "A Secure Scuttlebutt client written in C that is
compatible with the sbot command line tool")
     (home-page "https://git.scuttlebot.io/%25133ulDgs%2FoC1DXjoK04vDFy6DgVBB%2FZok15YJmuhD5Q%3D.sha256")
     (license #f))))
