(define-module (raingloom packages plan9)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix hg-download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix licenses:)
  #:use-module (gnu packages)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages base)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config)
  #:use-module (ice-9 ftw))

(define-public drawterm-9front
  (let ((commit "6b68ed2b23245b734c5b149c6556eda6e0f406c6")
        (revision "0"))
  (package
    (name "drawterm-9front")
    (version (string-append "0.1-" revision "." (substring commit 0 7)))
    (source (origin
	      (method hg-fetch)
	      (uri (hg-reference
		    (url "https://code.9front.org/hg/drawterm/")
		    (changeset commit)))
              (patches
               (parameterize
                   ((%patch-path
                     (map (lambda (directory)
                            (string-append directory "/raingloom/packages/patches"))
                          %load-path)))
               (search-patches "drawterm-pulseaudio.patch")))
	      (sha256 (base32 "0qmh8yy90ypdvaz2nlmcjm5wnggxdsn7naf31m4m20mw9drsinfy"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:phases
       (modify-phases
	%standard-phases
	(replace 'configure
		 (lambda _
		   (setenv "CONF" "unix")))
	(replace 'install
		 (lambda* (#:key inputs outputs #:allow-other-keys)
		   (invoke "install" "-Dm755" "drawterm" (string-append (assoc-ref outputs "out") "/bin/drawterm")))))))
    
    (inputs
     `(("xorg-server" ,xorg-server)
       ("libxt" ,libxt)))
    (synopsis "drawterm is a program that users of non-Plan 9 systems can use to establish graphical cpu connections with Plan 9 cpu servers")
    (description "This is a fork of Russ Cox's drawterm to incorporate features from Plan9front (http://9front.org), most importantly DP9IK authentication support (see authsrv(6)) and the TLS based rcpu(1) protocol.")
    (home-page "http://drawterm.9front.org/")
    (license licenses:gpl3))))

(define-public plan9port
  (let ((commit "e995a0c101863688d5f14649ae3de45a7c43789c")
        (revision "0"))
  (package
    (name "plan9port")
    (version (string-append "0.1-" revision "." (substring commit 0 7)))
    (source (origin
	      (method git-fetch)
	      (uri (git-reference
		    (url "https://github.com/9fans/plan9port")
		    (commit commit)))
	      (sha256 (base32 "03icp9aclf878g5ifnafdg7y968l61d0s2h2bhnrfbyrf2ry6f3f"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:modules ((ice-9 ftw)
		  (guix build utils)
		  (guix build gnu-build-system))
       #:phases
       (modify-phases
	%standard-phases
	(replace 'configure
		 (lambda* (#:key inputs outputs #:allow-other-keys) (and
		   (let ((p9dir (string-append (assoc-ref outputs "out") "/plan9")))
		     (setenv "PLAN9" (getcwd))
		     (setenv "PLAN9_TARGET" p9dir)

		     ;need these to trick src/cmd/devdraw/mkwsysrules.sh into not trying to detect X11 in /include and stuff
		     (setenv "WSYSTYPE" "x11")
		     (setenv "X11" "noX11dir")

		     ;src/cmd/fontsrv needs this
		     ;(setenv "C_INCLUDE_PATH" (string-append (getenv "C_INCLUDE_PATH") ":" (assoc-ref inputs "freetype") "/include/freetype2"))
		     (setenv "X11H" (string-append "-I" (assoc-ref inputs "freetype") "/include/freetype2"))

		     (with-output-to-file "config"
		       (lambda _
			 (display "FONTSRV=fontsrv\n")
			 (display "SYSVERSION=2.6.x\n")))

		     
		     (file-system-fold
		      (lambda (path stat result) ; enter?
			(not (member (basename path) '(".git" "font"))))
		      (lambda (path stat result) ; leaf
			(when (not (member (car (last-pair (string-split (basename path) #\.))) '("png" "pdf")))
			  (invoke "sed" "--in-place" "-e" (string-append "s,/usr/local/plan9," p9dir ",g") "--" path)
			  #t))
		      (lambda _ #t) ; down
		      (lambda _ #t) ; up
		      (lambda _ #t) ; skip
		      (lambda (error name stat errno result) ; error
			(format (current-error-port) "warning: ~a: ~a~%"
				name (strerror errno))
			#t)
		      #t
		      (getcwd))
		     (substitute* "src/cmd/fontsrv/x11.c"
				  (("<ft2build.h>") "<freetype2/ft2build.h>"))
		     ))))
	(replace 'build
		 (lambda _
		   (invoke "mkdir" "--parents" (getenv "PLAN9"))
		   (invoke "chmod" "--recursive" "u+w" "--" (getcwd) (getenv "PLAN9"))
		   (setenv "PATH" (string-append (getcwd) "/bin" ":" (getenv "PATH")))
		   (chdir "src")
		   (invoke "../dist/buildmk")
		   (invoke "mk" "clean")
		   (invoke "mk" "libs-nuke")
		   (invoke "mk" "all")
		   (invoke "mk" "-k" "install")
		   (copy-file "../bin/quote1" "../bin/\"")
		   (copy-file "../bin/quote2" "../bin/\"\"")
		   (invoke "mk" "clean")
		   (chdir "..")))
	(replace 'install
		 (lambda* (#:key inputs outputs #:allow-other-keys)
		   (let ((out (assoc-ref outputs "out")))
		     (invoke "u" "mkdir" "--parents" "--" (string-append out "/bin"))
		     (invoke "u" "cp" "--recursive" "--preserve=all" "--no-target-directory" "." (getenv "PLAN9_TARGET"))
		     (invoke "u" "ln" "--symbolic" (string-append (getenv "PLAN9_TARGET") "/bin/9") (string-append out "/bin"))))))))
    (propagated-inputs
     `(("xorg-server" ,xorg-server)
       ("freetype" ,freetype)
       ("libxt" ,libxt)
       ("libxext" ,libxext)
       ("which" ,which)
       ("perl" ,perl)
       ("fontconfig" ,fontconfig)
       ("fuse" ,fuse)))
    (synopsis "A port of many Plan 9 libraries and programs to Unix.")
    (description "Plan 9 from User Space (aka plan9port) is a port of many Plan 9 programs from their native Plan 9 environment to Unix-like operating systems.")
    (home-page "https://9fans.github.io/plan9port/")
    (license #f)))) ;; FIXME: package Lucent license
