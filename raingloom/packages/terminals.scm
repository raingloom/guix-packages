(define-module (raingloom packages terminals)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix hg-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xorg))

(define-public mlterm
  (package
   (name "mlterm")
   (version "3.8.9")
   (source
	(origin
	 (method hg-fetch)
	 (uri (hg-reference
		   ;; rel-3_8_9
		   (url "http://bitbucket.org/arakiken/mlterm")
		   (changeset (string-append "rel-"
									 (list->string (map (lambda (c)
														  (case c ((#\.) #\_)
																(else c)))
														(string->list version)))))))
	 (sha256
	  (base32
	   "18nfjaybbvfbzi903sjifvrq6mnvn5dniz91j1pvr6pfnv0lgk12"))))
   (build-system gnu-build-system)
   (arguments
	'(#:tests? #f ;; sadface
	  #:phases
	  (modify-phases
	   %standard-phases
	   ;; gross.
	   (add-after 'unpack 'make-source-writable
				  (lambda _
					(invoke "chmod" "--recursive" "--verbose" "u+w" ".")))
	   (add-after 'make-source-writable 'patch-bad-scripts
				  (lambda _
					(substitute* "tool/mlconfig/po/Makefile.in.in"
								 (("cd ..srcdir. && rm -f ...lang..gmo.*") ""))
					(for-each
					 (lambda (f)
					   (substitute* f
									(("-L/usr/local/lib -R/usr/local/lib") "")))
					 (list "main/Makefile.in"
						   "tool/mlfc/Makefile.in"
						   "tool/mlimgloader/Makefile.in"
						   "tool/mlconfig/Makefile.in"
						   "uitoolkit/libtype/Makefile.in"
						   "uitoolkit/libotl/Makefile.in"))
					(for-each (lambda (f)
								(substitute* f
											 (("-m 2755 -g utmp") " ")
											 (("-m 4755 -o root") " ")))
							  (list "configure.in"
									"configure")))))))
   (inputs
	`(("libx11" ,libx11)
	  ("cairo" ,cairo)
	  ("libxft" ,libxft)
	  ("gtk+" ,gtk+)
	  ("harfbuzz" ,harfbuzz)
	  ("fribidi" ,fribidi)
	  ("m17n-lib" ,m17n-lib)
	  ("openssl" ,openssl)
	  ("libssh2" ,libssh2)
	  ("gdk-pixbuf" ,gdk-pixbuf)
	  ("vte" ,vte)))
   (native-inputs
	`(("pkg-config" ,pkg-config)
	  ("gettext" ,gnu-gettext)
	  ("autoconf" ,autoconf)))
   (home-page "http://mlterm.sourceforge.net/")
   (synopsis "Mlterm is a multilingual terminal emulator on X11.")
   (description "mlterm is a multi-lingual terminal emulator written from scratch, which
supports various character sets and encodings in the world and  complex
characters  such  as  double  width for East Asian, combining for Thai,
Vietnamese, and so on, and bi-direction for Arabic and  Hebrew.   Indic
scripts  are experimentally supported.  It also supports various unique
feature such as anti-alias using FreeType, multiple XIM, multiple  windows,
scrollbar  API,  scroll  by  mouse wheel, automatic selection of
encoding, daemon mode, and so on.")
   (license #f ;;FIXME
			)))
