(define-module (raingloom packages linux)
  #:use-module (guix packages)
  #:use-module (gnu packages linux)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1))

(define-public linux-libre-nopae
  (package
   (inherit linux-libre)
   (name "linux-libre-nopae")
   (native-inputs
	`(("kconfig" ,(local-file
				   "aux-files/5.4-i686-noPAE.conf"))
	  ,@(alist-delete "kconfig"
					  (package-native-inputs linux-libre))))))
