;; thanks samplet and reepca from #guix on freenode, especially samplet who actually got this to build
(define-module (raingloom packages mininet))
(use-modules 
   (gnu packages networking)
   (gnu packages admin)
   (gnu packages python)
   (gnu packages python-xyz)
   (gnu packages linux)
   (gnu packages man)
   (gnu packages bash)
   (gnu packages gcc)
   (gnu packages)
   (guix packages)
   (guix git-download)
   (guix build-system gnu)
   (guix build-system python)
   (guix licenses))

(define-public mininet
  (package
   (name "mininet")
   (version "2.3.0d6")
   (source (origin
            (method git-fetch)
            (uri (git-reference
		  (url "git://github.com/mininet/mininet.git")
		  (commit version)))
	    (sha256 (base32 "0wc6gni9dxj9jjnw66a28jdvcfm8bxv1i776m5dh002bn5wjcl6x"))
	    (file-name (git-file-name name version))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f
      #:python ,python-2.7
      #:phases
      (let* ((gnu-phases (@ (guix build gnu-build-system) %standard-phases)))
	(modify-phases %standard-phases
		       (add-before 'build 'configure
				   (lambda* (#:key outputs #:allow-other-keys)
				     (let* ((out (assoc-ref outputs "out"))
					    (bin (string-append out "/bin"))
					    (man1 (string-append out "/share/man/man1")))
				       (mkdir-p bin)
				       (mkdir-p man1)
				       (substitute* "Makefile"
						    (("/usr/bin") bin)
						    (("/usr/share/man/man1") man1)
						    (("all: codecheck test") "all: mnexec")
						    (("cc") "gcc"))
				       #t)))
		       (add-before 'build 'build-mnexec (assoc-ref gnu-phases 'build))))))
   (inputs
    `(("openvswitch" ,openvswitch)
      ("python2" ,python-2.7)
      ("python2-pyflakes" ,python2-pyflakes)
      ("ethtool" ,ethtool)
      ("inetutils" ,inetutils)
      ("net-tools" ,net-tools)
      ("psmisc" ,psmisc)
      ("libcgroup" ,libcgroup)
      ("iproute2" ,iproute)
      ("iperf" ,iperf)
      ("bash" ,bash)))
   (native-inputs
    `(("help2man" ,help2man)))
   (synopsis "Rapid Prototyping for Software Defined Networks")
   (description "Mininet emulates a complete network of hosts, links, and switches on a single machine.")
   (home-page "http://mininet.org/")
   (license #f)))
